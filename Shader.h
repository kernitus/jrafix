#ifndef JRAPHICS_SHADER_H
#define JRAPHICS_SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <GL/glut.h>
#include <glm/glm.hpp>

class Shader {
public:
    Shader(const std::string &vertexName, const std::string &fragmentName);
    GLuint id;
    void activate();
    void setBool(const std::string &name, bool value) const;
    void setInt(const std::string &name, int value) const;
    void setFloat(const std::string &name, float value) const;
    void setMat4(const std::string &name, const glm::mat4 &mat) const;
    void setVec3(const std::string &name, const glm::vec3 &vec) const;
    void setVec3(const std::string &name, float x, float y, float z) const;
};


#endif //JRAPHICS_SHADER_H
