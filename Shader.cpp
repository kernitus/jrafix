#include <GL/glew.h>
#include "Shader.h"

Shader::Shader(const std::string &vertexName, const std::string &fragmentName) {
    std::string vertexCode, fragmentCode;
    std::ifstream vShaderFile, fShaderFile;

    try {
        vShaderFile.open(vertexName);
        fShaderFile.open(fragmentName);

        std::stringstream vShaderStream, fShaderStream;
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();

        vShaderFile.close();
        fShaderFile.close();

        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (std::ifstream::failure &e) {
        std::cout << "Could not load shader code" << std::endl;
    }

    const char *vShaderCode = vertexCode.c_str();
    const char *fShaderCode = fragmentCode.c_str();

    GLuint vertex, fragment;
    GLint success;
    GLchar infoLog[1024];

    // Vertex Shader
    vertex = glCreateShader(GL_VERTEX_SHADER);

    if (!vertex) {
        fprintf(stderr, "Error creating vertex shader\n");
        exit(0);
    }

    glShaderSource(vertex, 1, &vShaderCode, nullptr);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex, sizeof(infoLog), nullptr, infoLog);
        fprintf(stderr, "Error compiling vertex shader\n");
        fprintf(stderr, "%s", infoLog);
        exit(1);
    }

    // Fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, nullptr);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment, sizeof(infoLog), nullptr, infoLog);
        fprintf(stderr, "Error compiling fragment shader\n");
        fprintf(stderr, "%s", infoLog);
        exit(1);
    }

    id = glCreateProgram();
    glAttachShader(id, vertex);
    glAttachShader(id, fragment);
    glLinkProgram(id);

    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(id, sizeof(infoLog), nullptr, infoLog);
        fprintf(stderr, "Error linking shader program\n");
        exit(1);
    }

    // Program has been successfully linked but needs to be validated to check whether the program can execute given the current pipeline state
    glValidateProgram(id);
    glGetProgramiv(id, GL_VALIDATE_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(id, sizeof(infoLog), nullptr, infoLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", infoLog);
        exit(1);
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

void Shader::activate() {
    glUseProgram(id);
}

void Shader::setBool(const std::string &name, bool value) const {
    glUniform1i(glGetUniformLocation(id, name.c_str()), (int) value);
}

void Shader::setInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, float value) const {
    glUniform1f(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::setMat4(const std::string &name, const glm::mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setVec3(const std::string &name, const glm::vec3 &vec) const {
    glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, &vec[0]);
}

void Shader::setVec3(const std::string &name, float x, float y, float z) const {
    setVec3(name, glm::vec3(x, y, z));
}
