#include "Mesh.h"

#include <utility>

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures) {
    this->vertices = std::move(vertices);
    this->indices = std::move(indices);
    this->textures = std::move(textures);

    setupMesh();
}

void Mesh::setupMesh() {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);

    // vertex normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, normal));

    // bone ids
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(2, BONES_NUM, GL_INT, sizeof(Vertex), (void *) offsetof(Vertex, boneIds));

    // bone weights
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, BONES_NUM, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, boneWeights));

    // texture coordinates
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, textureCoords));

    glBindVertexArray(0);
}

void Mesh::draw(Shader &shader) {
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    GLuint normalNr = 1;
    GLuint heightNr = 1;

    for (GLint i = 0; i < textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        GLuint number = 0;
        std::string name = textures[i].type;
        if (name == "texture_diffuse") number = diffuseNr++;
        else if (name == "texture_specular") number = specularNr++;
        else if (name == "texture_normal") number = normalNr++;
        else if (name == "texture_height") number = heightNr++;

        glUniform1i(glGetUniformLocation(shader.id, (name + std::to_string(number)).c_str()), i);
        glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }

    // Draw the mesh
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);

    // Set back to default
    glActiveTexture(GL_TEXTURE0);
}
