#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <memory>
#include <chrono>
#include "Camera.h"
#include "entities/Entity.h"
#include "entities/AnimatedEntity.h"
#include "entities/Skybox.h"
#include "boids/boids.h"

// Timing
auto lastTime = std::chrono::high_resolution_clock::now();
bool moveBoids = false;

// Camera controls
Camera camera = Camera();
const float WIDTH = 1920;
const float HEIGHT = 1080;

struct MousePosition {
    int lastX = 0;
    int lastY = 0;
};
MousePosition mousePosition = MousePosition();

// Entities
auto entities = std::vector<Entity *>();
Skybox *skybox;

std::vector<Boid> boidsVector;
Boids *boids;
auto boidEntities = std::vector<AnimatedEntity *>();

void init() {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    skybox = new Skybox();

    auto fish = new AnimatedEntity("../models/fish.dae");
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            auto circus = new AnimatedEntity(*fish);
            circus->translate(4 * i, 2, 10 * j);
            boidsVector.emplace_back(BoidVec(4 * i, 2, 10 * j), BoidVec(0, 0, 0));
            entities.push_back(circus);
            boidEntities.push_back(circus);
        }
    }

    boids = new Boids(&boidsVector);

    auto barracuda = new AnimatedEntity("../models/barracuda.dae");
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            auto circus = new AnimatedEntity(*barracuda);
            circus->rotate(0,180,0);
            circus->translate(4 * i, 0, 10 * j);
            boidsVector.emplace_back(BoidVec(4 * i, 0, 10 * j), BoidVec(0, 0, 0));
            entities.push_back(circus);
            boidEntities.push_back(circus);
        }
    }

    auto clown = new AnimatedEntity("../models/clown.dae");
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            auto circus = new AnimatedEntity(*clown);
            circus->scale(0.03,0.03,0.03);
            circus->rotate(90,0,180);
            circus->translate(4 * i, 0, 10 * j);
            boidsVector.emplace_back(BoidVec(4 * i, 0, 10 * j), BoidVec(0, 0, 0));
            entities.push_back(circus);
            boidEntities.push_back(circus);
        }
    }

    auto water = new Entity("../models/water.dae", "../shaders/static.vs", "../shaders/static.fs");
    water->translate(0, -10, 0);
    water->scale(100, 0, 100);
    water->rotate(90, 0, 0);
    entities.push_back(water);

}

void display() {
    glClearColor(0.30f, 0.53f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // view/projection transformations
    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), WIDTH / HEIGHT, 0.1f, 200.0f);
    glm::mat4 view = camera.getViewMatrix();

    for (auto &item: entities) {
        item->display(projection, view, camera.Position, camera.Front);
    }

    skybox->display(projection, view, camera.getViewMatrix());

    glutSwapBuffers();
}

void idle() {
    auto finishTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> elapsed_seconds = finishTime - lastTime;
    lastTime = finishTime;
    float diff = elapsed_seconds.count();

    for (auto &item: entities) {
        item->updateAnimations(diff);
    }

    if(moveBoids) {
        boids->update(diff);

        for (int i = 0; i < boidsVector.size(); i++) {
            auto boid = boidsVector[i];
            auto pAnimatedEntity = boidEntities[i];
            boid.position += boid.velocity * diff;
            pAnimatedEntity->moveto(boid.position.X, boid.position.Y, boid.position.Z);
        }
    }

    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        // Camera movement
        case 'w':
            camera.processKeyboard(FORWARD);
            break;
        case 'a':
            camera.processKeyboard(LEFT);
            break;
        case 's':
            camera.processKeyboard(BACKWARD);
            break;
        case 'd':
            camera.processKeyboard(RIGHT);
            break;
        case 'f':
            for (const auto &item: entities) item->torch = !item->torch;
            break;
        case 'p':
            moveBoids = !moveBoids;
            break;
    }
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
    switch (button) {
        case 3:
            camera.processKeyboard(UP);
            break;
        case 4:
            camera.processKeyboard(DOWN);
            break;
    }
    glutPostRedisplay();
}

void mouseMovement(int x, int y) {
    float xoffset = x - mousePosition.lastX;
    float yoffset = mousePosition.lastY - y; // reversed since y-coordinates go from bottom to top

    mousePosition.lastX = x;
    mousePosition.lastY = y;

    camera.processMouseMovement(xoffset, yoffset);
    glutPostRedisplay();
}

void passiveMouseMovement(int x, int y) {
    mousePosition.lastX = x;
    mousePosition.lastY = y;
}

int main(int argc, char **argv) {
    // Set up the window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("Beautiful scene");

    // Register glut callback functions
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMovement);
    glutPassiveMotionFunc(passiveMouseMovement);
    glutIdleFunc(idle);

    // Call glewInit before initialising glut
    GLenum res = glewInit();
    // Check for any errors
    if (res != GLEW_OK) {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }
    // Set upobjects and shaders
    init();
    // Begin infinite event loop
    glutMainLoop();
    return 0;
}











