#include "Entity.h"

Entity::Entity(std::string const &modelPath, std::string const &vertexShaderPath,
               std::string const &fragmentShaderPath) {
    shader = new Shader(vertexShaderPath, fragmentShaderPath);
    model = new Model(modelPath);
}

Entity::Entity(Model &model, const std::string &vertexShaderPath, const std::string &fragmentShaderPath) {
    shader = new Shader(vertexShaderPath, fragmentShaderPath);
    this->model = &model;
}

Entity::~Entity() {
    delete shader;
    delete model;
}

void Entity::display(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition, glm::vec3 cameraFront) {
    shader->activate();

    // Vertex shader uniforms
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);
    shader->setMat4("model", position);

    // Fragment shader uniforms
    shader->setVec3("viewPos", cameraPosition);
    shader->setFloat("material.shininess", 32.0f);

    glm::vec3 pointLightPositions[] = {
            glm::vec3(-5.0f, 5.0f, -5.0f),
            glm::vec3(5.0f, 5.0f, 5.0f),
            glm::vec3(-4.0f, 2.0f, -12.0f),
            glm::vec3(10.0f, 0.0f, -3.0f)
    };
    // directional light
    shader->setVec3("dirLight.direction", -0.2f, -1.0f, -0.3f);
    shader->setVec3("dirLight.ambient", 0.08f, 0.05f, 0.05f);
    shader->setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
    shader->setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);
    // point light 1
    shader->setVec3("pointLights[0].position", pointLightPositions[0]);
    shader->setVec3("pointLights[0].ambient", 0.1f, 0.1f, 0.1f);
    shader->setVec3("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
    shader->setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
    shader->setFloat("pointLights[0].constant", 1.0f);
    shader->setFloat("pointLights[0].linear", 0.09);
    shader->setFloat("pointLights[0].quadratic", 0.032);
    // point light 2
    shader->setVec3("pointLights[1].position", pointLightPositions[1]);
    shader->setVec3("pointLights[1].ambient", 0.1f, 0.1f, 0.1f);
    shader->setVec3("pointLights[1].diffuse", 0.8f, 0.8f, 0.8f);
    shader->setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
    shader->setFloat("pointLights[1].constant", 1.0f);
    shader->setFloat("pointLights[1].linear", 0.09);
    shader->setFloat("pointLights[1].quadratic", 0.032);
    // point light 3
    shader->setVec3("pointLights[2].position", pointLightPositions[2]);
    shader->setVec3("pointLights[2].ambient", 0.1f, 0.1f, 0.1f);
    shader->setVec3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
    shader->setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
    shader->setFloat("pointLights[2].constant", 1.0f);
    shader->setFloat("pointLights[2].linear", 0.09);
    shader->setFloat("pointLights[2].quadratic", 0.032);
    // point light 4
    shader->setVec3("pointLights[3].position", pointLightPositions[3]);
    shader->setVec3("pointLights[3].ambient", 0.1f, 0.1f, 0.1f);
    shader->setVec3("pointLights[3].diffuse", 0.8f, 0.8f, 0.8f);
    shader->setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
    shader->setFloat("pointLights[3].constant", 1.0f);
    shader->setFloat("pointLights[3].linear", 0.09);
    shader->setFloat("pointLights[3].quadratic", 0.032);
    // spotLight
    shader->setBool("spotLight.on",torch);
    shader->setVec3("spotLight.position", cameraPosition);
    shader->setVec3("spotLight.direction", cameraFront);
    shader->setVec3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    shader->setVec3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
    shader->setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);
    shader->setFloat("spotLight.constant", 1.0f);
    shader->setFloat("spotLight.linear", 0.02);
    shader->setFloat("spotLight.quadratic", 0.005);
    shader->setFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
    shader->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

    model->draw(*shader);
}


void Entity::translate(float x, float y, float z) {
    position = glm::translate(position, glm::vec3(x, y, z));
}

void Entity::rotate(float x, float y, float z) {
    position = glm::rotate(position, glm::radians(x), glm::vec3(1.0, 0.0, 0.0));
    position = glm::rotate(position, glm::radians(y), glm::vec3(0.0, 1.0, 0.0));
    position = glm::rotate(position, glm::radians(z), glm::vec3(0.0, 0.0, 1.0));
    rotation = glm::vec3(x,y,z);
}

void Entity::scale(float x, float y, float z) {
    position = glm::scale(position, glm::vec3(x, y, z));
    scala = glm::vec3(x,y,z);
}

void Entity::moveto(float x, float y, float z){
    glm::mat4 identity = glm::mat4(1.0f);
    identity = glm::rotate(identity, glm::radians(rotation.x), glm::vec3(1.0, 0.0, 0.0));
    identity = glm::rotate(identity, glm::radians(rotation.y), glm::vec3(0.0, 1.0, 0.0));
    identity = glm::rotate(identity, glm::radians(rotation.z), glm::vec3(0.0, 0.0, 1.0));
    position = glm::translate(identity, glm::vec3(x,y,z));
    position = glm::scale(position, scala);
}