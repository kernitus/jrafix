#ifndef JRAPHICS_ENTITY_H
#define JRAPHICS_ENTITY_H

#include "../Model.h"

class Entity {
private:
    glm::mat4 position = glm::mat4(1.0f);
    glm::vec3 rotation = glm::vec3(0.0f);
    glm::vec3 scala = glm::vec3(1.0f);
protected:
    Shader *shader;
    Model *model;
public:
    bool torch = false;
    Entity(std::string const &modelPath, std::string const &vertexShaderPath, std::string const &fragmentShaderPath);
    Entity(Model &model, std::string const &vertexShaderPath, std::string const &fragmentShaderPath);
    ~Entity();
    void display(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition, glm::vec3 cameraFront);
    void translate(float x, float y, float z);
    void rotate(float x, float y, float z);
    void scale(float x, float y, float z);
    void moveto(float x, float y, float z);
    virtual void updateAnimations(float timeDifference){}
};

#endif //JRAPHICS_ENTITY_H
