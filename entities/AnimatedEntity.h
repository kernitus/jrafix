#ifndef JRAPHICS_ANIMATEDENTITY_H
#define JRAPHICS_ANIMATEDENTITY_H

#include <utility>
#include "../animations/Animator.h"
#include "../animations/Animation.h"

class AnimatedEntity : public Entity {
private:
    Animation *animation;
    Animator *animator;
public:
    explicit AnimatedEntity(const std::string &modelPath) :
            Entity(modelPath, "../shaders/animated.vs", "../shaders/static.fs") {
        animation = new Animation(modelPath, model);
        animator = new Animator(animation);
    }

    AnimatedEntity(const AnimatedEntity &obj) : Entity(*obj.model,"../shaders/animated.vs","../shaders/static.fs"){
        animation = obj.animation;
        animator = new Animator(animation);
    }

    ~AnimatedEntity(){
        delete animator;
        delete animation;
    }

    void updateAnimations(float timeDifference) override {
        shader->activate();
        animator->updateAnimation(timeDifference);

        auto transforms = animator->getPoseTransforms();
        for (int i = 0; i < transforms.size(); ++i)
            shader->setMat4("finalBonesMatrices[" + std::to_string(i) + "]", transforms[i]);
    }
};

#endif //JRAPHICS_ANIMATEDENTITY_H
