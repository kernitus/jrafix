#ifndef JRAPHICS_BOIDS_H
#define JRAPHICS_BOIDS_H

#include "boid.h"

class Boids {
public:
    float perceptionRadius = 100;
    float separationWeight = 1;
    float alignmentWeight = 0.3;
    float cohesionWeight = 0.8;
    float steeringWeight = 0.1;
    std::vector<BoidVec> steeringTargets;
    float blindspotAngleDeg = 20;
    float maxAcceleration = 3;
    float maxVelocity = 7;

    explicit Boids(std::vector<Boid> *entities) : boids(entities) {
        std::random_device rd;
        eng = std::mt19937(rd());
    }

    void update(float delta) {
        blindspotAngleDegCompareValue = cosf(M_PI * 2 * blindspotAngleDeg / 360.0f);
        updateAcceleration();

        for (auto &b: *boids) {
            b.velocity = (b.velocity + b.acceleration * delta).clampLength(maxVelocity);
            b.position += b.velocity * delta;
        }
    }

    void updateAcceleration() {
        if (perceptionRadius == 0) perceptionRadius = 1;
        makeCubeStore();
        for (auto &b: *boids)
            updateBoid(b);
    }

private:
    std::vector<Boid> *boids;
    std::unordered_map<BoidVec, std::vector<Boid *>, BoidVecHasher> cubeStore;
    std::mt19937 eng;
    float blindspotAngleDegCompareValue = 0;

    void updateBoid(Boid &b) {
        BoidVec separationSum;
        BoidVec headingSum;
        BoidVec positionSum;

        auto nearby = getCloseBoids(b);

        for (CloseBoid &closeBoid: nearby) {
            if (closeBoid.distance == 0) {
                separationSum += BoidVec::getRandomNumber(eng) * 1000;
            } else {
                float separationFactor = std::pow(closeBoid.distance, 2);
                separationFactor = separationFactor == 0 ? 0 : 1 / separationFactor;
                separationSum += closeBoid.direction.negative() * separationFactor;
            }
            headingSum += closeBoid.boid->velocity;
            positionSum += closeBoid.boid->position;
        }

        BoidVec steeringTarget = b.position;
        float targetDistance = -1;
        for (auto &target: steeringTargets) {
            float distance = target.distanceTo(b.position);
            if (targetDistance < 0 || distance < targetDistance) {
                steeringTarget = target;
                targetDistance = distance;
            }
        }

        BoidVec separation = !nearby.empty() ? separationSum / nearby.size() : separationSum;

        BoidVec alignment = !nearby.empty() ? headingSum / nearby.size() : headingSum;

        BoidVec avgPosition = !nearby.empty() ? positionSum / nearby.size() : b.position;
        BoidVec cohesion = avgPosition - b.position;

        BoidVec steering = (steeringTarget - b.position).normalised() * targetDistance;

        BoidVec bounds = BoidVec(0,0,0);

        if(b.position.X < -20) bounds.X = 3;
        else if(b.position.X > 20) bounds.X = -3;
        if(b.position.Y < -9) bounds.Y = 3;
        else if(b.position.Y > 20) bounds.Y = -3;
        if(b.position.Z < -20) bounds.Z = 3;
        else if(b.position.Z > 20) bounds.Z = -3;

        BoidVec acceleration;
        acceleration += separation * separationWeight;
        acceleration += alignment * alignmentWeight;
        acceleration += cohesion * cohesionWeight;
        acceleration += steering * steeringWeight;
        acceleration += bounds;
        b.acceleration = acceleration.clampLength(maxAcceleration);
    }

    std::vector<CloseBoid> getCloseBoids(const Boid &b) const {
        std::vector<CloseBoid> result;
        result.reserve(boids->size());

        BoidVec voxelPos = getBoidCube(b);
        voxelPos.X -= 1;
        voxelPos.Y -= 1;
        voxelPos.Z -= 1;
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                for (int z = 0; z < 3; z++) {
                    checkBoidCube(b, result, voxelPos);
                    voxelPos.Z++;
                }
                voxelPos.Z -= 3;
                voxelPos.Y++;
            }
            voxelPos.Y -= 3;
            voxelPos.X++;
        }
        return result;
    }

    void checkBoidCube(const Boid &b, std::vector<CloseBoid> &result, const BoidVec &voxelPos) const {
        auto iter = cubeStore.find(voxelPos);
        if (iter != cubeStore.end()) {
            for (Boid *test: iter->second) {
                const BoidVec &p1 = b.position;
                const BoidVec &p2 = test->position;
                BoidVec vec = p2 - p1;
                float distance = vec.length();

                float compareValue = 0;
                float l1 = vec.length();
                float l2 = b.velocity.length();
                if (l1 != 0 && l2 != 0) {
                    compareValue = b.velocity.negative().dotProduct(vec) / (l1 * l2);
                }

                if ((&b) != test && distance <= perceptionRadius &&
                    (blindspotAngleDegCompareValue > compareValue || b.velocity.length() == 0)) {
                    CloseBoid nb;
                    nb.boid = test;
                    nb.distance = distance;
                    nb.direction = vec;
                    result.push_back(nb);
                }
            }
        }
    }

    void makeCubeStore() {
        cubeStore.clear();
        cubeStore.reserve(boids->size());
        for (auto &b: *boids) {
            cubeStore[getBoidCube(b)].push_back(&b);
        }
    }

    BoidVec getBoidCube(const Boid &b) const {
        float r = std::abs(perceptionRadius);
        const BoidVec &p = b.position;
        BoidVec voxelPos;
        voxelPos.X = static_cast<int>(p.X / r);
        voxelPos.Y = static_cast<int>(p.Y / r);
        voxelPos.Z = static_cast<int>(p.Z / r);
        return voxelPos;
    }

};

#endif //JRAPHICS_BOIDS_H
