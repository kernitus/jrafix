#ifndef JRAPHICS_BOID_H
#define JRAPHICS_BOID_H

#include "maths.h"

struct Boid {
    BoidVec position;
    BoidVec velocity;
    BoidVec acceleration;

    explicit Boid(BoidVec pos, BoidVec vel) : position(pos), velocity(vel) {}
};

struct CloseBoid {
    Boid *boid;
    BoidVec direction;
    float distance;
};
#endif //JRAPHICS_BOID_H
