#ifndef JRAPHICS_MATHS_H
#define JRAPHICS_MATHS_H

#include <vector>
#include <unordered_map>
#include <cmath>
#include <random>

struct BoidVec {
    float X;
    float Y;
    float Z;

    explicit BoidVec(float x = 0, float y = 0, float z = 0) : X(x), Y(y), Z(z) {
    }

    float distanceTo(const BoidVec &other) const {
        return std::sqrt(std::pow(other.X - X, 2) + std::pow(other.Y - Y, 2) + std::pow(other.Z - Z, 2));
    }

    float length() const {
        return sqrtf(powf(X, 2) + powf(Y, 2) + powf(Z, 2));
    }

    float dotProduct(const BoidVec &v) const {
        return X * v.X + Y * v.Y + Z * v.Z;
    }

    BoidVec normalised() const {
        float length = this->length();
        if (length == 0) {
            return BoidVec();
        }
        return BoidVec(X / length, Y / length, Z / length);
    }

    BoidVec negative() const {
        return BoidVec(-X, -Y, -Z);
    }

    BoidVec &operator+=(const BoidVec &other) {
        X += other.X;
        Y += other.Y;
        Z += other.Z;
        return *this;
    }

    BoidVec operator/(float scalar) const {
        return BoidVec(X / scalar, Y / scalar, Z / scalar);
    }

    BoidVec operator*(float scalar) const {
        return BoidVec(X * scalar, Y * scalar, Z * scalar);
    }

    BoidVec operator+(const BoidVec &other) const {
        return BoidVec(X + other.X, Y + other.Y, Z + other.Z);
    }

    BoidVec operator-(const BoidVec &other) const {
        return BoidVec(X - other.X, Y - other.Y, Z - other.Z);
    }

    bool operator==(const BoidVec &rhs) const {
        return X == rhs.X && Y == rhs.Y && Z == rhs.Z;
    }

    [[nodiscard]] BoidVec clampLength(float length) const {
        return (this->length() > length) ? normalised() * length : *this;
    }

    static BoidVec getRandomNumber(std::mt19937 &engine) {
        std::uniform_real_distribution<float> thetaRange(0.0f, M_PI * 2);
        std::uniform_real_distribution<float> oneRange(0, 1);
        float theta = thetaRange(engine);
        float r = sqrt(oneRange(engine));
        float z = sqrt(1.0f - r * r) * (oneRange(engine) > 0.5f ? -1.0f : 1.0f);
        return BoidVec(r * cos(theta), r * sin(theta), z);
    }
};

struct BoidVecHasher {
    typedef std::size_t result_type;

    result_type operator()(BoidVec const &v) const {
        result_type const h1(std::hash<float>()(v.X));
        result_type const h2(std::hash<float>()(v.Y));
        result_type const h3(std::hash<float>()(v.Z));
        return (h1 * 31 + h2) * 31 + h3;
    }
};

#endif //JRAPHICS_MATHS_H
