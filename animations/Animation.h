#ifndef JRAPHICS_ANIMATION_H
#define JRAPHICS_ANIMATION_H

#include <string>
#include <vector>
#include "Bone.h"
#include "../Model.h"

struct AssimpNodeData {
    glm::mat4 transformation;
    std::string name;
    int childrenCount;
    std::vector<AssimpNodeData> children;
};

class Animation {
public:
    Animation(const std::string &animationPath, Model *model) {
        Assimp::Importer importer;
        const aiScene *scene = importer.ReadFile(animationPath, aiProcess_Triangulate);
        assert(scene && scene->mRootNode);
        auto animation = scene->mAnimations[0];
        m_Duration = animation->mDuration;
        m_TicksPerSecond = animation->mTicksPerSecond;
        readHierarchyData(m_RootNode, scene->mRootNode);
        setupBones(animation, *model);
    }

    Bone *findBone(const std::string &name) {
        auto iter = std::find_if(m_Bones.begin(), m_Bones.end(),
                                 [&](const Bone &Bone) {
                                     return Bone.getBoneName() == name;
                                 }
        );
        if (iter == m_Bones.end()) return nullptr;
        else return &(*iter);
    }


    inline float getTicksPerSecond() { return m_TicksPerSecond; }

    inline float getDuration() { return m_Duration; }

    inline const AssimpNodeData &getRootNode() { return m_RootNode; }

    inline const std::map<std::string, Model::BoneInfo> &getBoneIdMap() {
        return m_BoneInfoMap;
    }

private:
    float m_Duration;
    float m_TicksPerSecond;
    std::vector<Bone> m_Bones;
    AssimpNodeData m_RootNode;
    std::map<std::string, Model::BoneInfo> m_BoneInfoMap;

    void setupBones(const aiAnimation *animation, Model &model) {
        int size = animation->mNumChannels;

        auto &boneInfoMap = model.getOffsetMatMap();
        int &boneCount = model.GetBoneCount();

        for (int i = 0; i < size; i++) {
            auto channel = animation->mChannels[i];
            std::string boneName = channel->mNodeName.data;

            if (boneInfoMap.find(boneName) == boneInfoMap.end()) {
                boneInfoMap[boneName].id = boneCount;
                boneCount++;
            }
            m_Bones.emplace_back(channel->mNodeName.data, channel);
        }

        m_BoneInfoMap = boneInfoMap;
    }

    void readHierarchyData(AssimpNodeData &dest, const aiNode *src) {
        assert(src);

        dest.name = src->mName.data;
        dest.transformation = convertMatrixToGlmFormat(src->mTransformation);
        dest.childrenCount = src->mNumChildren;

        for (int i = 0; i < src->mNumChildren; i++) {
            AssimpNodeData newData;
            readHierarchyData(newData, src->mChildren[i]);
            dest.children.push_back(newData);
        }
    }
};

#endif //JRAPHICS_ANIMATION_H
