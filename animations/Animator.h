#ifndef JRAPHICS_ANIMATOR_H
#define JRAPHICS_ANIMATOR_H

#include "Animation.h"

class Animator {
public:
    explicit Animator(Animation *current) {
        m_CurrentAnimation = current;
        m_CurrentTime = 0;
        m_Transforms.reserve(100);
        for (int i = 0; i < 100; i++)
            m_Transforms.emplace_back(1.0f);
    }

    void updateAnimation(float dt) {
        if (m_CurrentAnimation) {
            m_CurrentTime += m_CurrentAnimation->getTicksPerSecond() * dt;
            m_CurrentTime = fmod(m_CurrentTime, m_CurrentAnimation->getDuration());
            calculateBoneTransform(&m_CurrentAnimation->getRootNode(), glm::mat4(1.0f));
        }
    }

    void calculateBoneTransform(const AssimpNodeData *node, glm::mat4 parentTransform) {
        std::string nodeName = node->name;
        glm::mat4 nodeTransform = node->transformation;

        Bone *Bone = m_CurrentAnimation->findBone(nodeName);

        if (Bone) {
            Bone->update(m_CurrentTime);
            nodeTransform = Bone->getLocalTransform();
        }

        glm::mat4 globalTransformation = parentTransform * nodeTransform;

        auto boneInfoMap = m_CurrentAnimation->getBoneIdMap();
        if (boneInfoMap.find(nodeName) != boneInfoMap.end()) {
            int index = boneInfoMap[nodeName].id;
            glm::mat4 offset = boneInfoMap[nodeName].offset;
            m_Transforms[index] = globalTransformation * offset;
        }

        for (int i = 0; i < node->childrenCount; i++)
            calculateBoneTransform(&node->children[i], globalTransformation);
    }

    std::vector<glm::mat4> getPoseTransforms() {
        return m_Transforms;
    }

private:
    std::vector<glm::mat4> m_Transforms;
    Animation *m_CurrentAnimation;
    float m_CurrentTime;
};

#endif //JRAPHICS_ANIMATOR_H
