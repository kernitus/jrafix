#ifndef JRAPHICS_MODEL_H
#define JRAPHICS_MODEL_H

#include <GL/glew.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <stb/stb_image.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "Shader.h"
#include "Mesh.h"
#include "SDL.h"
#include "utils/GLMUtils.h"

class Model {
public:
    explicit Model(std::string const &path);

    void draw(Shader &shader);

    struct BoneInfo {
        int id;
        glm::mat4 offset;
    };

    auto& getOffsetMatMap() { return m_OffsetMatMap; }

    int& GetBoneCount() { return m_BoneCount; }

private:
    std::string directory;
    std::vector<Mesh::Texture> loadedTextures;
    std::vector<Mesh> meshes;

    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);

    std::map<std::string, BoneInfo> m_OffsetMatMap;
    int m_BoneCount = 0;

    void setVertexBoneDataToDefault(Mesh::Vertex& vertex);
    void setVertexBoneData(Mesh::Vertex& vertex, int boneID, float weight);
    void extractBoneWeightForVertices(std::vector<Mesh::Vertex> &vertices, aiMesh *mesh);

    std::vector<Mesh::Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);

    GLuint textureFromFile(const char *path);
};

#endif //JRAPHICS_MODEL_H
