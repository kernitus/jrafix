Underwater OpenGL C++ scene with animated crowds of fish moving around following the Boids algorithm.

# OpenGL features

## Texture mapping
Each Entity class contains a Model, and each model has a set of textures that are loaded up from jpg/png files stored in the same directory as the models. These textures are applied to each individual mesh making up the model when it is drawn on screen.
![Textures](images/textures.png)

## Blinn-Phong illumination
The scene is lit using the Blinn-Phong illumination model. There are 4 point lights scattered around the scene, one directional light, and a spotlight (torch). The direction and position, ambient, diffuse, and specular components are set as uniforms in the code, and are all combined to achieve the resulting shading. For the point and directional lights, attenuation components are also set, so as to have a softer light coming through and not overwhelm the scene. There are 3 species of fish, a ground plane, and the skybox all with different material properties, and specular maps to determine the reflectivity.
![Phong](images/phong.png)

## Skeletal animations
All models were rigged in Blender by adding bones where needed, then moved and keyframed for each position, creating an animation. The model is loaded in the code and all bone positions extracted, including the position at each keyframe. The code then interpolates between one keyframe and the next for each timepoint to ensure smooth movement. Each bone position is a transformation matrix applied in the vertex shader.

![](images/skeletal.mp4)

## Boids algorithm
Each school of fish moves independently and interacts with the others as boids. Each fish’s position, velocity and acceleration is kept track of as 3D vectors. Each time period, the position of all the boids are updated by applying a set of rules, i.e. moving towards the centre of mass of the school, avoiding other fish, trying to match acceleration with nearby fish, and trying to stay within specific bounds. From the rules, the acceleration is updated, and thus the velocity. Finally, the fish are drawn in the new positions.

![](images/boids.mp4)

## Cube mapping
A water texture is used to provide a skybox, giving the user the illusion of being underwater. Images are loaded up as a cube to surround the camera viewpoint, and sampled using the GLSL’s shader sampleCube function.
![Skybox](images/skybox.png)
