#ifndef JRAPHICS_MESH_H
#define JRAPHICS_MESH_H

#define BONES_NUM 4

#include <GL/glew.h>
#include <vector>
#include "Shader.h"
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Mesh {
public:
    struct Vertex {
        glm::vec3 position, normal;
        int boneIds[BONES_NUM];
        float boneWeights[BONES_NUM];
        glm::vec2 textureCoords;
    };

    struct Texture {
        GLuint id;
        std::string type, path;
    };

    std::vector<Vertex> vertices;
    std::vector<Texture> textures;
    std::vector<GLuint> indices;

    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures);

    void draw(Shader &shader);

private:
    unsigned int VAO, VBO, EBO;

    void setupMesh();
};


#endif //JRAPHICS_MESH_H
