#version 430 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in ivec4 boneIds;
layout (location = 3) in vec4 weights;
layout (location = 4) in vec2 aTexCoords;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

const int MAX_BONES = 100;
const int BONES_NUM = 4;
uniform mat4 finalBonesMatrices[MAX_BONES];

void main(){
    vec4 totalPosition = vec4(0.0f);
    vec3 totalNormal = vec3(0.0f);

    for(int i = 0 ; i < BONES_NUM ; i++){
        if(boneIds[i] == -1) continue;
        if(boneIds[i] >=MAX_BONES){
            totalPosition = vec4(aPos,1.0f);
            break;
        }
        vec4 localPosition = finalBonesMatrices[boneIds[i]] * vec4(aPos,1.0f);
        totalPosition += localPosition * weights[i];
        vec3 localNormal = mat3(finalBonesMatrices[boneIds[i]]) * aNormal;
        totalNormal += localNormal;
    }

    Normal = mat3(transpose(inverse(model))) * totalNormal;
    FragPos = vec3(model * totalPosition);
    TexCoords = aTexCoords;
    gl_Position =  projection * view * model * totalPosition;
}
